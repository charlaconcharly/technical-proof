package registerUsers;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import common.CloseDriver;
import common.Constant;
import common.DriverControl;
import common.Functions;
import common.SelectDriver;
import objectPages.DemoPage;

import org.testng.AssertJUnit;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.junit.gen5.api.BeforeAll;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SampleTest {
	DemoPage demoObject;
	int currentSpin = 0;

	@Test(priority = 1)
	public void openBrowser() {
		String browser = "firefox";
		SelectDriver.openBrowser(DriverControl.getDriver(), browser);
		boolean correctBrowser = SelectDriver.checkIfBrowserIsCorrect(DriverControl.getDriver(), browser);
		AssertJUnit.assertTrue("ERROR: The opened browser is different to: " + browser, correctBrowser);
	}

	@Test(dependsOnMethods = { "openBrowser" }, priority = 2)
	public void openURL() {
		DriverControl.getDriver().get("http://demoqa.com/registration/");
		AssertJUnit.assertEquals("http://demoqa.com/registration/", DriverControl.getDriver().getCurrentUrl());

	}

	@Test(dependsOnMethods = { "openURL" }, priority = 3)
	public void defineDemoObject() {
		demoObject = PageFactory.initElements(DriverControl.getDriver(), DemoPage.class);
	}

	@Test(priority = 4)
	public void gettingDataUsernames() {
		demoObject.setListPositionsToBeUsed(demoObject.getListPositionsFromOtherList(Constant.supplierNames, 5));
		demoObject.setListLoggedFirstName(demoObject.getUsedFirstNames(demoObject.getListPositionsToBeUsed()));
		demoObject.setListLoggedLastName(demoObject.getUsedLastNames(demoObject.getListPositionsToBeUsed()));
		demoObject.setListLoggedUsername(demoObject.generateRandomUsernames(5));
		demoObject.setListNotLoggedFirstName(demoObject.getNOUsedFirstNames(demoObject.getListPositionsToBeUsed()));
		demoObject.setListNotLoggedLastName(demoObject.getNOUsedLastNames(demoObject.getListPositionsToBeUsed()));
	}

	@Test(priority = 5, invocationCount = 5)
	public void selectElements() throws InterruptedException {

		AssertJUnit.assertNotNull("There is not first names field.", demoObject.getEl_FirstName());
		demoObject.chooseRandomFirstName(DriverControl.getDriver(), demoObject.getEl_FirstName(),
				demoObject.getListLoggedFirstName().get(currentSpin));

		AssertJUnit.assertNotNull("There is not last names field.", demoObject.getEl_LastName());
		demoObject.chooseRandomLastName(DriverControl.getDriver(), demoObject.getEl_LastName(),
				demoObject.getListLoggedLastName().get(currentSpin));

		AssertJUnit.assertNotNull("There is not marital status.", demoObject.getEls_MaritalStatus());
		demoObject.clickRandomOption(demoObject.getEls_MaritalStatus());

		AssertJUnit.assertNotNull("There is not hobby.", demoObject.getEls_MaritalStatus());
		demoObject.clickRandomCheckBox(demoObject.getEls_Hobby());

		AssertJUnit.assertNotNull("There is not Country field.", demoObject.getEls_Country());
		demoObject.selectDropDown(demoObject.getEls_Country(), false);

		AssertJUnit.assertNotNull("There is not Month field.", demoObject.getEls_Month());
		demoObject.selectDropDown(demoObject.getEls_Month(), true);

		AssertJUnit.assertNotNull("There is not Day field.", demoObject.getEls_Day());
		demoObject.selectDropDown(demoObject.getEls_Day(), true);

		AssertJUnit.assertNotNull("There is not Year field.", demoObject.getEls_Year());
		demoObject.selectDropDown(demoObject.getEls_Year(), true);

		AssertJUnit.assertNotNull("There is not Phone field.", demoObject.getEl_Phone());
		demoObject.chooseRandomPhone(DriverControl.getDriver(), demoObject.getEl_Phone());

		AssertJUnit.assertNotNull("There is not Username field.", demoObject.getEl_Username());
		demoObject.chooseRandomUsername(DriverControl.getDriver(), demoObject.getEl_Username(),
				demoObject.getListLoggedUsername().get(currentSpin));

		AssertJUnit.assertNotNull("There is not Email field.", demoObject.getEl_Email());
		demoObject.chooseRandomEmail(DriverControl.getDriver(), demoObject.getEl_Email());

		AssertJUnit.assertNotNull("There is not Profile Pics field.", demoObject.getEl_ProfilePic());
		AssertJUnit.assertEquals("file", demoObject.getEl_ProfilePic().getAttribute("type"));
		demoObject.chooseRandomPicture(DriverControl.getDriver(), demoObject.getEl_ProfilePic());

		AssertJUnit.assertNotNull("There is not Description field.", demoObject.getEl_Description());
		demoObject.chooseRandomDescription(DriverControl.getDriver(), demoObject.getEl_Description());

		AssertJUnit.assertNotNull("There is not Password field.", demoObject.getEl_Password());
		AssertJUnit.assertNotNull("There is not PasswordConfirm field.", demoObject.getEl_PasswordConfirm());
		demoObject.chooseRandomPassword(DriverControl.getDriver(), demoObject.getEl_Password(),
				demoObject.getEl_PasswordConfirm());

		AssertJUnit.assertNotNull("There is not Submit button.", demoObject.getEl_Submit());
		demoObject.getEl_Submit().click();

		try {
			if (demoObject.getEl_Error().isDisplayed()) {
				// System.out.println("Displayed");
				demoObject.setListLoggedUsername(currentSpin, demoObject.generateRandomUsernames(1).get(0));
				openURL();
				selectElements();
			} else {
				// It will go to Exception
			}
		} catch (Exception e) {
			System.out.println("The Error does not exist, so that is good. ");
			currentSpin++;
			openURL();
		}

	}

	@Test(priority = 6)
	public void showUsernames() {
		for (int x = 0; x < demoObject.getListLoggedUsername().size(); x++) {
			System.out.println("Used Username: " + demoObject.getListLoggedUsername().get(x));
			System.out.println("Used FirstName: " + demoObject.getListLoggedFirstName().get(x));
			System.out.println("Used LastName: " + demoObject.getListLoggedLastName().get(x));
		}

		for (int x = 0; x < demoObject.getListNotLoggedFirstName().size(); x++) {
			System.out.println("NO Used FirstName: " + demoObject.getListNotLoggedFirstName().get(x));
			System.out.println("NO Used LastName: " + demoObject.getListNotLoggedLastName().get(x));
		}
	}

	@Test(priority = 7)
	public void closeDriver() {
		try {
			CloseDriver.killProcess(DriverControl.getDriver());
		} catch (Exception e) {
			AssertJUnit.assertTrue(e.getMessage(), false);
		}

	}

}