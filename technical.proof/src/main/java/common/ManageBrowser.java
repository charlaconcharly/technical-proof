package common;

import org.openqa.selenium.WebDriver;

public class ManageBrowser {

    public static void startSize(WebDriver driver, String siz) {
        if (siz.equals("large")) {
            driver.manage().window().setSize(new org.openqa.selenium.Dimension(1680, 1050));
        } else if (siz.equals("medium")) {
            driver.manage().window().setSize(new org.openqa.selenium.Dimension(1024, 768));
        } else if (siz.equals("small")) {
            driver.manage().window().setSize(new org.openqa.selenium.Dimension(640, 576));
        } else if (siz.equals("max")) {
            driver.manage().window().maximize();
        }
        DriverControl.setDriver(driver);
    }

    public static String currentSize(WebDriver driver) {
        String size;
        if (driver.manage().window().getSize().height >= 1680 && driver.manage().window().getSize().width >= 1050) {
            size = "max";
        } else if (driver.manage().window().getSize().height >= 1024 && driver.manage().window().getSize().width >= 768) {
            size = "large";
        } else if (driver.manage().window().getSize().height >= 640 && driver.manage().window().getSize().width >= 576) {
            size = "medium";
        } else {
            size = "small";
        }
        return size;
    }

}