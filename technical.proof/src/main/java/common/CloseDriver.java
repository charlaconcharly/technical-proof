package common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.openqa.selenium.WebDriver;

public class CloseDriver {

	public static void killProcess(WebDriver driver) {
		
		String proceso = null;
		// This variable is used after Selenium 3 because is the complement to
		// init Firefox
		String procesoGecko = "geckodriver.exe";
		String[] tempString = null;
		Process p;
		String taskLine = "";
		BufferedReader input = null;
		try {
			p = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
			input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			taskLine = input.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (driver.getClass().toString().contains("InternetExplorer")) {
			proceso = "IEDriverServer.exe";
		} else if (driver.getClass().toString().contains("Chrome")) {
			proceso = "chromedriver.exe";
		} else if (driver.getClass().toString().contains("Firefox")) {
			proceso = "firefox.exe";
		}

		try {
			driver.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (proceso != null) {
				// Ejecuta un proceso en el cual me devuelve toda la lista de
				// los
				// procesos activos en este momento
				// El bucle se recorrera mientras no sea la ultima linea.
				// NOTA: tasklist.exe es el ultimo proceso ejecutado porque lo
				// hemos
				// hecho una linea antes. Lo mas normal es que no se habra un
				// navegador entre estas lineas
				
				while (taskLine.contains("tasklist.exe") == false) {
					tempString = taskLine.split(",", 0);
					// Se comprueba que el nombre del proceso es iexplorer.exe
					// que
					// corresponde al navegador Internet Explorer
					
					
					if (tempString[0].contains(proceso) || tempString[0].contains(procesoGecko)) {
						// Cierra el proceso por lo que cierra el navegador
						Runtime.getRuntime().exec("taskkill /pid " + tempString[1] + " /f");

					}
					
					
					
					
					/*
					if (tempString[0].contains(proceso) || tempString[0].contains(procesoGecko)
							|| tempString[0].contains("conhost.exe") || tempString[0].contains("cmd.exe")) {
						// Cierra el proceso por lo que cierra el navegador
						Runtime.getRuntime().exec("taskkill /pid " + tempString[1] + " /f");

					}
					*/
					// Lee la siguiente linea del tasklist
					taskLine = input.readLine();
				}
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
