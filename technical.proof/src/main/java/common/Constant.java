package common;

import java.util.Arrays;
import java.util.List;

public class Constant {

	public final static List<String> supplierNames = Arrays.asList("Jan van Dam", "Chack Norris", "Klark n Kent", "John Daw",
			"Bat Man", "Tim Los", "Dave o Core", "Pay Pal", "Lazy Cat", "Jack & Johnes");
	
	public final static String SIZ_MAX = "max";
	
	public final static String geckoPath = System.getProperty("user.dir") + "\\src\\main\\resources\\geckodriver.exe";
	public final static String chromePath = System.getProperty("user.dir") + "\\src\\main\\resources\\chromedriver.exe";
	public final static String iePath = System.getProperty("user.dir") + "\\src\\main\\resources\\IEDriverServer.exe";

}
