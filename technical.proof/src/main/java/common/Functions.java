package common;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Functions {

	public static int getRandomNumber(int limitMin, int limitMax) {
		Random rnd = new Random();
		return rnd.nextInt(limitMax - limitMin + 1) + limitMin;
	}

	public static String getRandomString(String charactersAllowed, int lengthString) {
		String characters = charactersAllowed;
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < lengthString) { // length of the random string.
			int index = (int) (rnd.nextFloat() * characters.length());
			salt.append(characters.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	public static String getRandomEmail() {
		return getRandomString("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", getRandomNumber(3, 15)) + "@mail.com";
	}

	public static String getRandomUsername() {
		return getRandomString("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_", getRandomNumber(6, 15));
	}

	public static String getRandomPassword() {
		return getRandomString(
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?",
				getRandomNumber(10, 20));
	}

	public static String getRandomDescription() {
		return getRandomString("ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890.", getRandomNumber(100, 500));
	}

	public static List<String> getFirstNames(List<String> list) {
		return getNames(list, "first");
	}

	public static List<String> getLastNames(List<String> list) {
		return getNames(list, "last");
	}

	public static List<String> getNames(List<String> list, String partOfName) {
		List<String> names = new ArrayList<String>();
		String currentElement = "";
		for (int x = 0; x < list.size(); x++) {
			currentElement = "";
			String[] splitNames = list.get(x).split(" ");
			if (partOfName.equalsIgnoreCase("first")) {
				currentElement = splitNames[0];
			} else if (partOfName.equalsIgnoreCase("last")) {
				currentElement = splitNames[splitNames.length - 1];
			}

			names.add(currentElement);
		}
		return names;
	}

	/**
	 * Function to write in a element (input tag) a text.
	 * 
	 * @param driver
	 *            It is the WebDriver where is the WebElement.
	 * @param element
	 *            It is the WebElement where the text will be written.
	 * @param text
	 *            It is the text that will be written in the Input element.
	 */
	public static void introduceText(WebDriver driver, WebElement element, String text) {
		JavascriptExecutor executorJS = (JavascriptExecutor) driver;
		String classDriver = driver.getClass().toString();
		String elementTag = element.getTagName();

		if (elementTag.equalsIgnoreCase("input") || elementTag.equalsIgnoreCase("textarea")) {
			element.clear();
			if (classDriver.contains("InternetExplorer")) {
				String currentXPath = getXPATH(element, "");
				String elementoJS = getElementByXPathInJavaScript(currentXPath);
				executorJS.executeScript("var elemento=" + elementoJS + "; elemento.value =\"" + text + "\";");
			} else {
				element.sendKeys(text);
			}
		}
	}

	/**
	 * Function that return the XPath (String) of the current WebElement
	 * 
	 * @param childElement
	 *            It is the WebElement that will return the XPath.
	 * @param current
	 *            It is the current XPath of the father element. It used to be
	 *            blank.
	 * @return It returns a String with the current XPath of the WebElement
	 *         passed.
	 */
	public static String getXPATH(WebElement childElement, String current) {
		String childTag = childElement.getTagName();
		if (childTag.equals("html")) {
			return "/html[1]" + current;
		}
		WebElement parentElement = childElement.findElement(By.xpath(".."));
		List<WebElement> childrenElements = parentElement.findElements(By.xpath("*"));
		int count = 0;
		for (int i = 0; i < childrenElements.size(); i++) {
			WebElement childrenElement = childrenElements.get(i);
			String childrenElementTag = childrenElement.getTagName();
			if (childTag.equals(childrenElementTag)) {
				count++;
			}
			if (childElement.equals(childrenElement)) {
				return getXPATH(parentElement, "/" + childTag + "[" + count + "]" + current);
			}
		}
		return null;
	}

	/**
	 * Function that return the string to execute in a JavascriptExecutor. This
	 * String will be used to get an element by XPath in JavaScript. Example:
	 * <p>
	 * JavascriptExecutor executorJS = (JavascriptExecutor) currentWebDriver;
	 * <br>
	 * executorJS.executeScript("var
	 * elemento="+getElementByXPathInJavaScript(currentXPathOfTheWebElement)+";");
	 * 
	 * @param path
	 *            It is the current Xpath of the element. It must be a String
	 *            type.
	 * @return It returns a String that will be used in a JavascriptExecutor to
	 *         get an element by XPath in JavaScript
	 */
	public static String getElementByXPathInJavaScript(String path) {
		return "document.evaluate('" + path
				+ "', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;";
	}

}
