package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;

import common.DriverControl;

public class SelectDriver {
	public SelectDriver() {
	}

	public static void openBrowser(WebDriver driver, String browser) {
		startBrowser(driver, browser);
		ManageBrowser.startSize(DriverControl.getDriver(), Constant.SIZ_MAX);
	}

	public static void startBrowser(WebDriver driver, String browser) {

		if (browser.equals("firefox")) {

			driver = startFirefox(driver);
		} else if (browser.equals("chrome")) {

			driver = startChrome(driver);
		} else if (browser.equals("ie")) {

			driver = startIE(driver);
		} else {

		}
		DriverControl.setDriver(driver);
		
		//checkIfBrowserIsCorrect(driver, browser);
	}

	private static WebDriver startIE(WebDriver driver) {
		try {
			System.setProperty("webdriver.ie.driver", Constant.iePath);
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			driver = new InternetExplorerDriver(options);

		} catch (Exception ex) {
			System.err.println("ERROR: " + ex.getMessage());

		}
		return driver;
		// http://toolsqa.com/selenium-webdriver/challenges-to-run-selenium-scripts-with-ie-browser/
		/*
		 * IMPORTANT: The zoom of IE11 must be 100%
		 */

		/*
		 * For IE 11 only, you will need to set a registry entry on the target
		 * computer so that the driver can maintain a connection to the instance
		 * of Internet Explorer it creates. For 32-bit Windows installations,
		 * the key you must examine in the registry editor is
		 * HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet
		 * Explorer\Main\FeatureControl\FEATURE_BFCACHE. For 64-bit Windows
		 * installations, the key is
		 * HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Internet
		 * Explorer\Main\FeatureControl\FEATURE_BFCACHE. Please note that the
		 * FEATURE_BFCACHE subkey may or may not be present, and should be
		 * created if it is not present. Important: Inside this key, create a
		 * DWORD value named iexplore.exe with the value of 0.
		 */
	}

	private static WebDriver startChrome(WebDriver driver) {
		System.setProperty("webdriver.chrome.driver", Constant.chromePath);
		driver = new ChromeDriver();
		return driver;
	}

	private static WebDriver startFirefox(WebDriver driver) {
		System.setProperty("webdriver.gecko.driver", Constant.geckoPath);

		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("intl.accept_languages", "es");

		driver = new FirefoxDriver();
		return driver;
	}

	public static boolean checkIfBrowserIsCorrect(WebDriver driver, String browser) {
		String classDriver = driver.getClass().toString();
		boolean correctBrowser = false;
		if (classDriver.contains("Firefox") && browser.equals("firefox")) {
			correctBrowser = true;
		} else if (classDriver.contains("Chrome") && browser.equals("chrome")) {
			correctBrowser = true;
		} else if (classDriver.contains("InternetExplorer") && browser.equals("ie")) {
			correctBrowser = true;
		}

		return correctBrowser;
	}

}