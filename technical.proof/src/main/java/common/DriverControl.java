package common;

import org.openqa.selenium.WebDriver;

public class DriverControl {
	static WebDriver driver;

	public static WebDriver getDriver() {
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		DriverControl.driver = driver;
	}

}
