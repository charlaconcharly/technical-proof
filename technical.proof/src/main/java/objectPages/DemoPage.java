package objectPages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;

import common.Constant;
import common.Functions;

public class DemoPage {
	public DemoPage() {
		super();
	}

	private List<Integer> listPositionsToBeUsed;
	private List<String> listLoggedUsername;
	private List<String> listLoggedFirstName;
	private List<String> listLoggedLastName;

	private List<String> listNotLoggedFirstName;
	private List<String> listNotLoggedLastName;

	private final static String id_FirstName = "name_3_firstname";
	private final static String id_LastName = "name_3_lastname";

	private final static String name_MaritalStatus = "radio_4[]";
	private final static String name_Hobby = "checkbox_5[]";

	private final static String id_Country = "dropdown_7";
	private final static String id_Month = "mm_date_8";
	private final static String id_Day = "dd_date_8";
	private final static String id_Year = "yy_date_8";
	private final static String tagName_InsideSelectTag = "option";

	private final static String id_Phone = "phone_9";
	private final static String id_Username = "username";
	private final static String id_Email = "email_1";

	private final static String id_ProfilePic = "profile_pic_10";
	private final static String id_Description = "description";

	private final static String id_Password = "password_2";
	private final static String id_PasswordConfirm = "confirm_password_password_2";

	private final static String name_Submit = "pie_submit";

	private final static String css_Error = "p.piereg_login_error>strong";

	@FindBy(id = id_FirstName)
	private WebElement el_FirstName;
	@FindBy(id = id_LastName)
	private WebElement el_LastName;

	@FindBys({ @FindBy(name = name_MaritalStatus) })
	private List<WebElement> els_MaritalStatus;

	@FindBys({ @FindBy(name = name_Hobby) })
	private List<WebElement> els_Hobby;

	@FindBys({ @FindBy(how = How.ID, using = id_Country), // This is like the
															// father
			@FindBy(tagName = tagName_InsideSelectTag) // This is like the child
	})
	private List<WebElement> els_Country;

	@FindBys({ @FindBy(how = How.ID, using = id_Month), // This is like the
			// father
			@FindBy(tagName = tagName_InsideSelectTag) // This is like the child
	})
	private List<WebElement> els_Month;

	@FindBys({ @FindBy(how = How.ID, using = id_Day), // This is like the
			// father
			@FindBy(tagName = tagName_InsideSelectTag) // This is like the child
	})
	private List<WebElement> els_Day;

	@FindBys({ @FindBy(how = How.ID, using = id_Year), // This is like the
			// father
			@FindBy(tagName = tagName_InsideSelectTag) // This is like the child
	})
	private List<WebElement> els_Year;

	@FindBy(id = id_Phone)
	private WebElement el_Phone;
	@FindBy(id = id_Username)
	private WebElement el_Username;
	@FindBy(id = id_Email)
	private WebElement el_Email;

	@FindBy(id = id_ProfilePic)
	private WebElement el_ProfilePic;

	@FindBy(id = id_Description)
	private WebElement el_Description;

	@FindBy(id = id_Password)
	private WebElement el_Password;
	@FindBy(id = id_PasswordConfirm)
	private WebElement el_PasswordConfirm;
	@FindBy(name = name_Submit)
	private WebElement el_Submit;

	@FindBy(css = css_Error)
	private WebElement el_Error;

	public void addListLoggedUsername(String stringToAdd) {
		if (!listLoggedUsername.contains(stringToAdd)) {
			List<String> listLoggedUsername = getListLoggedUsername();
			listLoggedUsername.add(stringToAdd);
			setListLoggedUsername(listLoggedUsername);
		}
	}
	
	public void setListLoggedUsername(int index, String element){
		List<String> listLoggedUsername = getListLoggedUsername();
		listLoggedUsername.set(index, element);
		setListLoggedUsername(listLoggedUsername);
	}

	public List<Integer> getListPositionsToBeUsed() {
		return listPositionsToBeUsed;
	}

	public void setListPositionsToBeUsed(List<Integer> listPositionsToBeUsed) {
		this.listPositionsToBeUsed = listPositionsToBeUsed;
	}

	public List<String> getListLoggedUsername() {
		return listLoggedUsername;
	}

	public void setListLoggedUsername(List<String> listLoggedUsername) {
		this.listLoggedUsername = listLoggedUsername;
	}

	public List<String> getListLoggedFirstName() {
		return listLoggedFirstName;
	}

	public void setListLoggedFirstName(List<String> listLoggedFirstName) {
		this.listLoggedFirstName = listLoggedFirstName;
	}

	public List<String> getListLoggedLastName() {
		return listLoggedLastName;
	}

	public void setListLoggedLastName(List<String> listLoggedLastName) {
		this.listLoggedLastName = listLoggedLastName;
	}

	public List<String> getListNotLoggedFirstName() {
		return listNotLoggedFirstName;
	}

	public void setListNotLoggedFirstName(List<String> listNotLoggedFirstName) {
		this.listNotLoggedFirstName = listNotLoggedFirstName;
	}

	public List<String> getListNotLoggedLastName() {
		return listNotLoggedLastName;
	}

	public void setListNotLoggedLastName(List<String> listNotLoggedLastName) {
		this.listNotLoggedLastName = listNotLoggedLastName;
	}

	public WebElement getEl_FirstName() {
		return el_FirstName;
	}

	public void setEl_FirstName(WebElement el_FirstName) {
		this.el_FirstName = el_FirstName;
	}

	public WebElement getEl_LastName() {
		return el_LastName;
	}

	public void setEl_LastName(WebElement el_LastName) {
		this.el_LastName = el_LastName;
	}

	public List<WebElement> getEls_MaritalStatus() {
		return els_MaritalStatus;
	}

	public void setEls_MaritalStatus(List<WebElement> els_MaritalStatus) {
		this.els_MaritalStatus = els_MaritalStatus;
	}

	public List<WebElement> getEls_Hobby() {
		return els_Hobby;
	}

	public void setEls_Hobby(List<WebElement> els_Hobby) {
		this.els_Hobby = els_Hobby;
	}

	public List<WebElement> getEls_Country() {
		return els_Country;
	}

	public void setEls_Country(List<WebElement> els_Country) {
		this.els_Country = els_Country;
	}

	public List<WebElement> getEls_Month() {
		return els_Month;
	}

	public void setEls_Month(List<WebElement> els_Month) {
		this.els_Month = els_Month;
	}

	public List<WebElement> getEls_Day() {
		return els_Day;
	}

	public void setEls_Day(List<WebElement> els_Day) {
		this.els_Day = els_Day;
	}

	public List<WebElement> getEls_Year() {
		return els_Year;
	}

	public void setEls_Year(List<WebElement> els_Year) {
		this.els_Year = els_Year;
	}

	public WebElement getEl_Phone() {
		return el_Phone;
	}

	public void setEl_Phone(WebElement el_Phone) {
		this.el_Phone = el_Phone;
	}

	public WebElement getEl_Username() {
		return el_Username;
	}

	public void setEl_Username(WebElement el_Username) {
		this.el_Username = el_Username;
	}

	public WebElement getEl_Email() {
		return el_Email;
	}

	public void setEl_Email(WebElement el_Email) {
		this.el_Email = el_Email;
	}

	public WebElement getEl_ProfilePic() {
		return el_ProfilePic;
	}

	public void setEl_ProfilePic(WebElement el_ProfilePic) {
		this.el_ProfilePic = el_ProfilePic;
	}

	public WebElement getEl_Description() {
		return el_Description;
	}

	public void setEl_Description(WebElement el_Description) {
		this.el_Description = el_Description;
	}

	public WebElement getEl_Password() {
		return el_Password;
	}

	public void setEl_Password(WebElement el_Password) {
		this.el_Password = el_Password;
	}

	public WebElement getEl_PasswordConfirm() {
		return el_PasswordConfirm;
	}

	public void setEl_PasswordConfirm(WebElement el_PasswordConfirm) {
		this.el_PasswordConfirm = el_PasswordConfirm;
	}

	public WebElement getEl_Submit() {
		return el_Submit;
	}

	public void setEl_Submit(WebElement el_Submit) {
		this.el_Submit = el_Submit;
	}

	public WebElement getEl_Error() {
		return el_Error;
	}

	public void setEl_Error(WebElement el_Error) {
		this.el_Error = el_Error;
	}

	public void clickRandomOption(List<WebElement> optionList) {
		if (optionList.size() > 1) {
			int i = Functions.getRandomNumber(0, (optionList.size() - 1));
			optionList.get(i).click();
		}
	}

	public void clickRandomCheckBox(List<WebElement> checkBoxList) throws InterruptedException {

		if (checkBoxList.size() > 0) {

			int numCheckBoxWillBeClicked = Functions.getRandomNumber(1, checkBoxList.size());
			List<Integer> listPositionsToBeClicked = new ArrayList<Integer>();
			List<WebElement> listToGet = checkBoxList;
			int maxElements = numCheckBoxWillBeClicked;

			listPositionsToBeClicked = getListPositionsFromOtherList(listToGet, maxElements);

			for (int i = 0; i < listPositionsToBeClicked.size(); i++) {
				checkBoxList.get(listPositionsToBeClicked.get(i)).click();
			}

		}
	}

	public void selectDropDown(List<WebElement> optionsDropDownList, boolean valueByDefect) {
		if (optionsDropDownList.size() > 0) {
			int initialPosition = 0;
			if (valueByDefect) {
				initialPosition = 1;
			}

			optionsDropDownList.get(Functions.getRandomNumber(initialPosition, (optionsDropDownList.size() - 1)))
					.click();
		}
	}

	public void chooseRandomPhone(WebDriver driver, WebElement phoneFieldElement) {
		Functions.introduceText(driver, phoneFieldElement,
				(String) ("00" + Functions.getRandomNumber(10000000, 99999999)));
	}

	public void chooseRandomEmail(WebDriver driver, WebElement emailFieldElement) {
		Functions.introduceText(driver, emailFieldElement, Functions.getRandomEmail());
	}

	public void chooseRandomFirstName(WebDriver driver, WebElement firstNameFieldElement, String firstName) {
		Functions.introduceText(driver, firstNameFieldElement, firstName);
	}

	public void chooseRandomLastName(WebDriver driver, WebElement lastNameFieldElement, String lastName) {
		Functions.introduceText(driver, lastNameFieldElement, lastName);
	}

	public void chooseRandomUsername(WebDriver driver, WebElement usernameFieldElement, String username) {
		Functions.introduceText(driver, usernameFieldElement, username);
	}

	public List<String> generateRandomUsernames(int maxElements) {
		List<String> listPositionsToBeUsed = new ArrayList<String>();

		String usernameToBeSaved;
		int currentIndex = 0;
		while (currentIndex < maxElements) {

			usernameToBeSaved = Functions.getRandomUsername();

			if (!listPositionsToBeUsed.contains(usernameToBeSaved)) {
				listPositionsToBeUsed.add(currentIndex, usernameToBeSaved);
				currentIndex++;
			}
		}

		return listPositionsToBeUsed;
	}

	public void chooseRandomPassword(WebDriver driver, WebElement passwordFieldElement,
			WebElement confirmPasswordFieldElement) {
		String randomPassword = Functions.getRandomPassword();
		Functions.introduceText(driver, passwordFieldElement, randomPassword);
		if (confirmPasswordFieldElement != null) {
			Functions.introduceText(driver, confirmPasswordFieldElement, randomPassword);
		}
	}

	public void chooseRandomPicture(WebDriver driver, WebElement pictureFieldElement) throws InterruptedException {
		if (pictureFieldElement.getAttribute("type").equalsIgnoreCase("file")) {
			pictureFieldElement.sendKeys(System.getProperty("user.dir") + "\\src\\test\\resources\\profilePics\\"
					+ Functions.getRandomNumber(1, 7) + ".png");
		}

	}

	public void chooseRandomDescription(WebDriver driver, WebElement descriptionFieldElement) {
		Functions.introduceText(driver, descriptionFieldElement, Functions.getRandomDescription());
	}

	public List<Integer> getListPositionsFromOtherList(List<?> listToGet, int maxElements) {

		List<Integer> listPositionsToBeUsed = new ArrayList<Integer>();

		if (maxElements <= listToGet.size()) {

			int positionToBeSaved;
			int currentIndex = 0;
			while (currentIndex < maxElements) {

				positionToBeSaved = Functions.getRandomNumber(0, (listToGet.size() - 1));

				if (!listPositionsToBeUsed.contains(positionToBeSaved)) {
					listPositionsToBeUsed.add(currentIndex, positionToBeSaved);
					currentIndex++;
				}
			}
		} else {
			System.err.println("It is not possible get more elements that the size of the list.");
		}

		return listPositionsToBeUsed;
	}

	public List<String> getUsedFirstNames(List<Integer> listPositionsToBeUsed) {
		List<String> firstNames = Functions.getFirstNames(Constant.supplierNames);
		List<String> firstNamesToBeLogged = new ArrayList<String>();
		for (int i = 0; i < listPositionsToBeUsed.size(); i++) {
			firstNamesToBeLogged.add(i, firstNames.get(listPositionsToBeUsed.get(i)));
		}
		return firstNamesToBeLogged;
	}

	public List<String> getUsedLastNames(List<Integer> listPositionsToBeUsed) {
		List<String> lastNames = Functions.getLastNames(Constant.supplierNames);
		List<String> lastNamesToBeLogged = new ArrayList<String>();
		for (int i = 0; i < listPositionsToBeUsed.size(); i++) {
			lastNamesToBeLogged.add(i, lastNames.get(listPositionsToBeUsed.get(i)));
		}
		return lastNamesToBeLogged;
	}

	public List<String> getNOUsedFirstNames(List<Integer> listPositionsToBeUsed) {
		List<String> firstNames = Functions.getFirstNames(Constant.supplierNames);
		List<String> firstNamesNOLogged = new ArrayList<String>();

		int indexToAdd = 0;
		for (int i = 0; i < Constant.supplierNames.size(); i++) {
			if (!listPositionsToBeUsed.contains(i)) {
				firstNamesNOLogged.add(indexToAdd, firstNames.get(i));
				indexToAdd++;
			}
		}
		return firstNamesNOLogged;
	}

	public List<String> getNOUsedLastNames(List<Integer> listPositionsToBeUsed) {
		List<String> lastNames = Functions.getLastNames(Constant.supplierNames);
		List<String> lastNamesNOLogged = new ArrayList<String>();

		int indexToAdd = 0;
		for (int i = 0; i < Constant.supplierNames.size(); i++) {
			if (!listPositionsToBeUsed.contains(i)) {
				lastNamesNOLogged.add(indexToAdd, lastNames.get(i));
				indexToAdd++;
			}
		}
		return lastNamesNOLogged;
	}

}
