Technical Proof

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---


## Resources
1. Java SE Development Kit 8 Update 161 (64-bit)
2. Eclipse Neon 3
3. Maven apache-maven-3.5.3

## To execute
1. Run As > Maven Builds...
2. Write on goal: **clean test**
3. It will execute the test on Maven

